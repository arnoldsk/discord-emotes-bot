/**
 * @todo split into components, add babel for that
 */
new Vue({
  el: "#app",

  data() {
    return {
      user: {},
      selectedGuild: {},
      emoteName: "",
      emoteUrl: "",
      maxFileBytes: 256000,
      uploadFileSize: 0,
      serverName: "",
      emotes: [],
      selectGuildError: "",
      uploadError: "",
      emoteUploadType: "url",
    };
  },

  computed: {
    userAvatarUrl() {
      const { id, avatar } = this.user;

      return avatar
        ? `https://cdn.discordapp.com/avatars/${id}/${avatar}`
        : "/assets/img/default.png"; // TODO: add full URL
    },

    maxfileKB() {
      return Math.floor(this.maxFileBytes / 1000);
    },

    uploadFileSizeValid() {
      return this.uploadFileSize <= this.maxFileBytes;
    },

    userOwnedGuilds() {
      return this.user.guilds.filter(guild => guild.owner);
    },
  },

  methods: {
    getGuildIconUrl(guildData) {
      const { id, icon } = guildData;

      return icon
        ? `https://cdn.discordapp.com/icons/${id}/${icon}`
        : "/assets/img/default.png"; // TODO: add full URL
    },

    /**
     * @todo disable selection while this is happening
     */
    selectGuild(guildData) {
      if (this.selectedGuild.id === guildData.id) {
        return;
      }

      this.$http
        .post("/guild", { guildId: guildData.id })
        .then(this.selectGuildSuccess, this.selectGuildFail);
    },

    selectGuildSuccess(response) {
      const data = response.body;

      if (data.success) {
        // Clear data
        this.selectGuildError = "";

        // Update selected guild data
        this.selectedGuild = data.guild;
      } else {
        this.selectGuildFail(response);
      }
    },

    selectGuildFail(response) {
      this.selectedGuild = {};
      this.selectGuildError = response.body.message;
    },

    onInputFileChange(e) {
      const input = e.target;

      // Update stored file size
      this.uploadFileSize = input.files.length ? input.files[0].size : 0;

      // Update name field value
      if (input.files.length) {
        const fileName = input.files[0].name;

        this.emoteName = fileName.split(".")[0];
      }
    },

    /**
     * @todo disable form while uploading
     */
    uploadFile() {
      if (!this.uploadFileSizeValid) {
        return;
      }

      // Send the file
      const input = this.$refs.uploadInput;
      const emoteFile = input.files.length ? input.files[0] : null;

      const formData = new FormData();
      formData.append("guildId", this.selectedGuild.id);
      formData.append("name", this.emoteName);
      formData.append("emoteFile", emoteFile);
      formData.append("emoteUrl", this.emoteUrl);
      formData.append("emoteUploadType", this.emoteUploadType);

      this.$http
        .post("/addEmote", formData)
        .then(this.uploadSuccess, this.uploadFail);
    },

    uploadSuccess(response) {
      const data = response.body;

      if (data.success) {
        // Clear data
        this.uploadError = "";
        this.emoteName = "";
        this.emoteUrl = "";
        this.$refs.uploadInput.value = "";

        // Update selected guild data
        this.selectedGuild = data.guild;
      } else {
        this.uploadFail(response);
      }
    },

    uploadFail(response) {
      this.uploadError = response.body.message;
    },
  },

  created() {
    this.user = USER;
  },
});
