const express = require("express");
const expressSession = require("express-session");
const bodyParser = require("body-parser");
const path = require("path");
const multer = require("multer");
const passport = require("passport");
const passportDiscord = require("passport-discord");

module.exports = (config, discord) => {
  const app = express();
  const upload = multer();
  const port = config.port || 3000;

  // Templating and static files
  app.set("view engine", "ejs");
  app.use(express.static("public"));

  // Read request body data
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  // Session
  app.use(
    expressSession({
      secret: "monkey dog",
      resave: false,
      saveUninitialized: false,
    }),
  );

  // Passport
  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((obj, done) => {
    done(null, obj);
  });

  passport.use(
    new passportDiscord.Strategy(
      {
        clientID: config.discord.clientId,
        clientSecret: config.discord.clientSecret,
        callbackURL: `${config.url}discord/callback`,
        scope: ["identify", "guilds"],
      },
      (accessToken, refreshToken, profile, done) => {
        process.nextTick(() => done(null, profile));
      },
    ),
  );

  app.use(passport.initialize());
  app.use(passport.session());

  // Middlewares
  const discordMiddleware = (req, res, next) => {
    if (!req.isAuthenticated()) {
      return res.redirect("/login");
    }

    return next();
  };

  // Routing
  app.get("/", discordMiddleware, (req, res) => {
    res.render(path.join(__dirname, "..", "public", "index"), {
      user: req.session.passport.user,
    });
  });

  app.get("/login", (req, res) => {
    res.render(path.join(__dirname, "..", "public", "login"), {
      url: `${config.url}discord/auth`,
    });
  });

  app.get("/logout", (req, res) => {
    req.logout();
    res.redirect("/login");
  });

  app.get(
    "/discord/auth",
    passport.authenticate("discord", {
      scope: ["identify", "guilds"],
    }),
    (req, res) => {},
  );

  app.get(
    "/discord/callback",
    passport.authenticate("discord", {
      failureRedirect: "/login",
    }),
    (req, res) => {
      res.redirect("/");
    },
  );

  app.post("/guild", discordMiddleware, (req, res) => {
    try {
      const guildId = req.body.guildId;

      if (!guildId) {
        throw new Error("Missing guild selection");
      }

      // Add emote to the server
      // Discord scope try catch
      try {
        const guild = discord.getGuild(guildId);

        if (!guild) {
          throw new Error(
            [
              "The bot has not been added to this server, unable to fetch emotes.",
              `Invite the bot and try again: <a href="${discord.getInviteUrl()}" target="_blank">${
                discord.client.user.username
              }</a>`,
            ].join("<br>"),
          );
        }

        res.status(200);
        res.json({
          success: true,
          message: "Guild data fetched",
          guild: discord.getGuildData(guild),
        });
      } catch (error) {
        throw new Error(`Discord error: ${error.message}`);
      }
    } catch (error) {
      res.status(406);
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

  app.post(
    "/addEmote",
    discordMiddleware,
    upload.single("emoteFile"),
    async (req, res) => {
      try {
        const guildId = req.body.guildId;
        const name = req.body.name;
        const emoteUploadType = req.body.emoteUploadType || "url";
        const emoteFile = req.file;
        const emoteUrl = req.body.emoteUrl || "";

        // Validate ID and Name
        if (!guildId || !name) {
          throw new Error("Missing guild selection or emote name");
        }

        // Validate URL
        if (emoteUploadType === "url") {
          if (
            !/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/.test(
              emoteUrl,
            )
          ) {
            throw new Error("Emote URL has to be a URL");
          }
        }

        let emoteSource = null;

        // Validate upload type data
        switch (emoteUploadType) {
          case "file":
            if (emoteFile) {
              emoteSource = emoteFile.buffer;
            } else {
              throw new Error("Missing emote file");
            }
            break;

          case "url":
            if (emoteUrl) {
              emoteSource = emoteUrl;
            } else {
              throw new Error("Missing emote URL");
            }
            break;

          default:
            throw new Error("Incorrect upload type - image/url");
        }

        // Add emote to the server
        // Discord scope try catch
        try {
          const guild = discord.getGuild(guildId);
          const emoji = await guild.createEmoji(emoteSource, name);

          res.status(200);
          res.json({
            success: true,
            message: `Emote :${emoji.name}: added to server "${guild.name}".`,
            guild: discord.getGuildData(guild),
          });
        } catch (error) {
          throw new Error(`Discord error: ${error.message}`);
        }
      } catch (error) {
        res.status(406);
        res.json({
          success: false,
          message: error.message,
        });
      }
    },
  );

  // Listen
  app.listen(port, () => {
    console.log(`Listening on *:${port}`);
  });
};
