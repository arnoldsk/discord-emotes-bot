/**
 * Application configuration mapping from .env file
 */
module.exports = {
  url: process.env.URL || "/",
  port: process.env.PORT || 3000,
  discord: {
    clientId: process.env.DISCORD_CLIENT_ID || "",
    clientSecret: process.env.DISCORD_CLIENT_SECRET || "",
    clientToken: process.env.DISCORD_CLIENT_TOKEN || "",
  },
};
