const Discord = require("discord.js");

module.exports = config => {
  const client = new Discord.Client();

  // Login
  client.login(config.discord.clientToken);

  // On ready announce it
  client.on("ready", () => {
    console.log(`Discord client user ${client.user.username} ready`);

    // Announce guild count
    console.log(`Active in: ${client.guilds.size} guild(s).`);
  });

  // Methods and variables
  const getGuild = id => client.guilds.find(guild => guild.id == id);

  const getGuildData = guild => {
    const emotes = guild.emojis.map(emote => ({
      name: emote.name,
      url: emote.url,
    }));

    return {
      id: guild.id,
      name: guild.name,
      emotes,
    };
  };

  const getInviteUrl = () =>
    `https://discordapp.com/oauth2/authorize?client_id=${
      client.user.id
    }&scope=bot&permissions=1073741824`;

  return {
    client,
    getGuild,
    getGuildData,
    getInviteUrl,
  };
};
