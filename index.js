require("dotenv").config();

const config = require("./src/config");
const discord = require("./src/discord")(config);

discord.client.on("ready", () => {
  require("./src/server")(config, discord);
});
